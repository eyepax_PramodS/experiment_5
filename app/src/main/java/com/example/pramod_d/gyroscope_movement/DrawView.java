package com.example.pramod_d.gyroscope_movement;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.media.Image;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;


/**
 * Created by pramod_d on 25/2/2016.
 */
public class DrawView extends View {
    float orientationRoll;
    float rollAngle;
    float picthAngle;
    float azumitAngle;
    float orientationPitch;
    float orientationazumit;
    float xVariable;
    float yVaraiable;
    Bitmap background;
    Image back;
    int reqHeight;
    int reqWidth;
    boolean is_set=false;
    boolean is_pitch=false;
    boolean is_azimut=false;
    boolean is_roll=false;
    public DrawView(Context context,View container){
        super(context);
        background= BitmapFactory.decodeResource(context.getResources(), R.drawable.koala);

        //orientationPitch=(container.getHeight()/2)-(background.getHeight()/2);
       // orientationRoll=(container.getWidth()/2)-(background.getWidth()/2);

        invalidate();

    }
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);
        if(!is_set){
            Matrix m = new Matrix();

//            reqHeight = canvas.getHeight(); //new required height
//            reqWidth = background.getWidth() * background.getHeight() / canvas.getHeight();    //new req. width
            reqHeight = 2884;//new required height
            reqWidth = 4288;    //new req. width

            m.setRectToRect(new RectF(0, 0, background.getWidth(), background.getHeight()), new RectF(0, 0, reqWidth, reqHeight), Matrix.ScaleToFit.CENTER); //here you can change the Matrix.ScaleToFit parameter

            background = Bitmap.createBitmap(background, 0, 0, background.getWidth(), background.getHeight(), m, true);
            is_set=true;

        }

       canvas.drawBitmap(background, orientationRoll, orientationPitch, null);

        if(is_pitch){
            canvas.drawBitmap(background,0,orientationPitch, null);

        }
       else if(is_azimut){
            canvas.drawBitmap(background,orientationazumit,yVaraiable, null);
            System.out.println("y variable:==========>"+yVaraiable);
        }
        else if(is_roll){
            canvas.drawBitmap(background,orientationRoll,yVaraiable, null);
        }

    }
    public void move(float rollAngle,float offsetWidth){
        this.rollAngle=rollAngle;
        this.picthAngle=picthAngle;
        //orientationPitch=picthAngle*(background.getHeight()/45)-background.getHeight()/2 + offsetHeight;
        orientationRoll=rollAngle*(background.getWidth()/360)-background.getWidth()/2 + offsetWidth;

        invalidate();
    }
    public void move_pitch(float picthAngle,float offsetWidth){
        this.picthAngle=picthAngle;
        orientationPitch=-1*(+picthAngle*(background.getHeight()/360)-background.getHeight()/2+offsetWidth);

        invalidate();
    }
    public void move_azumit(float azumitAngle ,float offSetWidth){
        this.azumitAngle=azumitAngle;
        orientationazumit=-1*(azumitAngle*(background.getWidth()/360)-background.getWidth()/2+offSetWidth);
        invalidate();

    }


}
