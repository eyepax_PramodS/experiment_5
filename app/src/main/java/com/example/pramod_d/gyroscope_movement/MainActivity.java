package com.example.pramod_d.gyroscope_movement;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements SensorEventListener{
    DrawView drawView;
    LinearLayout drawViewLayout;
    private SensorManager mSensorManager;
    Sensor accelerometer;
    Sensor magnetometer;
    float[] mGravity;
    float[] mGeomagnetic;
    private TextView xView,yView,zView;
    private float orientationValues[]=new float[3];
    ArrayList<Float> orientationValueList =new ArrayList<Float>();
    ArrayList<Float> pitchValueList =new ArrayList<Float>();
    ArrayList<Float> azumitValueList =new ArrayList<Float>();
    float Threshold=2f;
    float thresholdPitch=2f;
    float thresholdAzumit=2f;
    float pitchOffset=0;
    float rotationOffset=0;
    float azumithoffset=0;
    boolean firstValue=false;
    Button resetButton,pitchButton,azimutButton,rollButton;
    ArrayList<Float> azumitValueList_test =new ArrayList<Float>();
    ArrayList<Float> pitchValueList_test =new ArrayList<Float>();
    ArrayList<Float> rollValueList_test =new ArrayList<Float>();
    float sum;
    float sum_pitch;
    float sum_roll;
    int size=75,pitchFilterSize=50,rollFilterSize=50;
    int weight_1=1,weight_2=1,weight_3=2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        xView=(TextView)findViewById(R.id.textView);
        yView=(TextView)findViewById(R.id.textView3);
        zView=(TextView)findViewById(R.id.textView4);
        resetButton=(Button)findViewById(R.id.resetButton);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstValue=false;
            }
        });
        pitchButton=(Button)findViewById(R.id.pitchButton);
        pitchButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                drawView.is_pitch=true;
                drawView.is_roll=false;
                drawView.is_azimut=false;

            }

        });
        rollButton=(Button)findViewById(R.id.rollButton);
        rollButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                drawView.yVaraiable=drawView.orientationPitch;
                drawView.is_roll=true;
                drawView.is_pitch=false;
                drawView.is_azimut=false;
            }

        });
        azimutButton=(Button)findViewById(R.id.azimutButton);
        azimutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawView.yVaraiable = drawView.orientationPitch;
                drawView.is_azimut = true;
                drawView.is_roll = false;
                drawView.is_pitch = false;
            }

        });
        drawViewLayout=(LinearLayout)findViewById(R.id.drawViewLayout);
        drawView=new DrawView(this,drawViewLayout);
        drawViewLayout.addView(drawView);


    }
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);
    }
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }
    @Override
    public void onSensorChanged(SensorEvent event) {
        azumitValueList_test.clear();
        pitchValueList_test.clear();
        rollValueList_test.clear();
        xView.setText("Az:"+orientationValues[0]);
        yView.setText("Pi:"+orientationValues[1]);
        zView.setText("Ro:" + orientationValues[2]);

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = event.values;
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = event.values;
        if (mGravity != null && mGeomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];
            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                orientationValues[0]=orientation[0]*(180/(float)Math.PI);
                orientationValues[1]=orientation[1]*(180/(float)Math.PI);
                orientationValues[2]=orientation[2]*(180/(float)Math.PI);

               //azumith.........................................
                float tempAz=orientation[0]*(180/(float)Math.PI);
//                azumitValueList.add(tempAz<0?tempAz+360:tempAz);
//                if(tempAz<0){
//                    azumitValueList.add(tempAz+360);
//                }
//                else if(tempAz>-1 && tempAz<1){
//                    azumitValueList.add(0f);
//                }
//                else{
//                    azumitValueList.add(tempAz);
//                }
                azumitValueList.add(tempAz);
                //..............................pitch......................................
                float tempPt=(orientation[1] * (180 / (float) Math.PI));
//                if(tempPt<0){
//                    pitchValueList.add(tempPt+180);
//                }
//                else if(tempPt>-1 && tempPt<1){
//                    pitchValueList.add(0f);
//                }
//                else{
//                    pitchValueList.add(tempPt);
//                }
                pitchValueList.add(tempPt);
                //...................................Roll.............................................
                float tempRo=orientation[2]*(180/(float)Math.PI);
//                if(tempRo<0){
//                    orientationValueList.add(tempRo + 180);
//                }
//                else if(tempRo>-1 && tempRo<1){
//                    orientationValueList.add(0f);
//                }
//                else{
//                    orientationValueList.add(tempRo);
//                }
                orientationValueList.add(tempRo);
                System.out.println("orientaion values" + orientation);

                // orientation contains: azimut, pitch and roll
                if(!firstValue){
                    pitchOffset=-orientationValues[1];
                    rotationOffset=-orientationValues[2];
                    azumithoffset=-orientationValues[0];
                    firstValue=true;
                }
            }
            if(orientationValueList.size()==rollFilterSize){
                orientationValueList.remove(0);
            }
            if(pitchValueList.size()==pitchFilterSize){
                pitchValueList.remove(0);
            }
            if(azumitValueList.size()==size){
                azumitValueList.remove(0);
            }
//            if(azumitValueList_test.size()==3){
//                azumitValueList_test.remove(0);
//            }
        }
        //drawView.move_azumit(orientationValues[0]);
        //drawView.move_pitch(orientationValues[1]+pitchOffset,drawViewLayout.getWidth()/2);
       // drawView.move(orientationValues[2]+rotationOffset,drawViewLayout.getHeight()/2);
//        try{
//            drawView.move(orientationValues[2]);
//        }
//        catch(Exception e){
//            System.out.println("--");
//        }


//        for (int i = -180; i <=175 ; i=i+3) {
//            if(i< orientationValues[2] &&orientationValues[2]< i+2){
//                drawView.move(i*1f);
//            }
//        }

////        //bitmap slider
//       try{
//           if(orientationValueList.get(0)>0&&orientationValueList.get(1)>0||(orientationValueList.get(0)<0&&orientationValueList.get(1)<0)){
//               if(Math.abs(orientationValueList.get(0) - orientationValueList.get(1))>Threshold)
//                   drawView.move(orientationValueList.get(1) + rotationOffset, drawViewLayout.getHeight() / 2);
//           }
//
//           else if(orientationValueList.get(0)>0&&orientationValueList.get(1)<0||(orientationValueList.get(0)<0&&orientationValueList.get(1)>0)){
//               if(Math.abs(orientationValueList.get(0) +orientationValueList.get(1))>Threshold)
//                     drawView.move(orientationValueList.get(1) + rotationOffset, drawViewLayout.getHeight() / 2);
//           }
//
//       }
//       catch (Exception e) {
//           System.out.println("Error");
//       }
//        try{
//            if(pitchValueList.get(0)>0&&pitchValueList.get(1)>0||(pitchValueList.get(0)<0&&pitchValueList.get(1)<0)){
//                if(Math.abs(pitchValueList.get(0) - pitchValueList.get(1))>thresholdPitch)
//                    drawView.move_pitch(pitchValueList.get(1) + pitchOffset, drawViewLayout.getWidth() / 2);
//            }
//
//            else if(pitchValueList.get(0)>0&&pitchValueList.get(1)<0||(pitchValueList.get(0)<0&&pitchValueList.get(1)>0)){
//                if(Math.abs(pitchValueList.get(0) +pitchValueList.get(1))>thresholdPitch)
//                    drawView.move_pitch(pitchValueList.get(1) + pitchOffset, drawViewLayout.getWidth() / 2);
//            }
//
//        }
//        catch (Exception e) {
//            System.out.println("Error");
//        }
//        try{
//            if(azumitValueList.get(0)>0&&azumitValueList.get(1)>0||(azumitValueList.get(0)<0&&azumitValueList.get(1)<0)){
//                if(Math.abs(azumitValueList.get(0)-azumitValueList.get(1))>thresholdAzumit)
//                    drawView.move_azumit(azumitValueList.get(1));
//            }
//            if(azumitValueList.get(0)>0&&azumitValueList.get(1)<0||(azumitValueList.get(0)<0&&azumitValueList.get(1)>0)){
//                if(Math.abs(azumitValueList.get(0)+azumitValueList.get(1))>thresholdAzumit)
//                    drawView.move_azumit(azumitValueList.get(1));
//            }
//        }
//        catch (Exception e) {
//            System.out.println("Error");
//        }


      //  -----------------------------------------Flitering New method--------------------------------------------------------
        if(azumitValueList.size()==size-1){

                sum=0;
                for(int j=0;j<size-1;j++) {
                    sum += azumitValueList.get(j);

//                    if(j>=0 &&j<=35)
//                        sum+= azumitValueList.get(j)*weight_1;
//                    if(j>35 &&j<=45)
//                        sum+= azumitValueList.get(j)*weight_2;
//                    if(j>45 &&j<=49)
//                        sum+= azumitValueList.get(j)*weight_3;


                }
            azumitValueList_test.add(sum / size-1);
            System.out.println("Azumit list" + azumitValueList);
            System.out.println("Azumit average list====>" + azumitValueList_test);
            drawView.move_azumit(azumitValueList_test.get(0)+azumithoffset,4288/2);


        }
        if(pitchValueList.size()==pitchFilterSize-1){

            sum_pitch=0;
            for(int j=0;j<pitchFilterSize-1;j++) {
//                if(j>=0 &&j<=35)
//                    sum_pitch+= pitchValueList.get(j)*weight_1;
//                if(j>35 &&j<=45)
//                    sum_pitch+= pitchValueList.get(j)*weight_2;
//                if(j>45 &&j<=49)
//                    sum_pitch+= pitchValueList.get(j)*weight_3;
//

                sum_pitch+= pitchValueList.get(j);



            }
            pitchValueList_test.add(sum_pitch / (pitchFilterSize-1));
            System.out.println("Pitch list" + pitchValueList);
            System.out.println("Pitch average list====>" + pitchValueList_test);
            drawView.move_pitch(pitchValueList_test.get(0)+pitchOffset,2844);


        }
        if(orientationValueList.size()==rollFilterSize-1){

            sum_roll=0;
            for(int j=0;j<rollFilterSize-1;j++) {
//                if(j>=0 &&j<=35)
//                sum_roll+= orientationValueList.get(j)*weight_1;
//                if(j>35 &&j<=45)
//                    sum_roll+= orientationValueList.get(j)*weight_2;
//                if(j>45 &&j<=49)
//                    sum_roll+= orientationValueList.get(j)*weight_3;

                sum_roll+= orientationValueList.get(j);



            }
            rollValueList_test.add(sum_roll / rollFilterSize - 1);
            System.out.println("roll values list" + orientationValueList);
            System.out.println("roll average list====>" + rollValueList_test);
            drawView.move(rollValueList_test.get(0) +rotationOffset,drawViewLayout.getWidth()/2);


        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {


    }
}
